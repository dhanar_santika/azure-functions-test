using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Xunit;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Http;
using HelloWorld.Test.Helper;

namespace HelloWorld.Test
{
    public class HelloWorldTest
    {
        private readonly ILogger logger = TestFactory.CreateLogger();

        [Fact]
        public async void Http_Trigger_Should_Return_Hello_World()
        {
            var request = TestFactory.CreateHttpRequest("name", "hello");
            var response = (OkObjectResult)await App.HelloWorld.Run(request, logger);
            Assert.Equal("Hello, World!", response.Value);
        }

        [Fact]
        public async void Http_Trigger_Should_Return_Hello_Name()
        {
            var request = TestFactory.CreateHttpRequest("name", "dhanar");
            var response = (OkObjectResult)await App.Name.Run(request, logger);
            Assert.Equal("Hello, dhanar", response.Value);
        }

        [Fact]
        public async void Http_Trigger_Does_Not_Return()
        {
            var request = new DefaultHttpRequest(new DefaultHttpContext());
            var response = (BadRequestObjectResult)await App.Name.Run(request, logger);
            Assert.Equal("Hello, anonymous", response.Value);
        }
    }
}
