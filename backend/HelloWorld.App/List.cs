using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace HelloWorld.App
{
    public static class List
    {
        [FunctionName("List")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "List/{number}")] HttpRequest req,
            ILogger log, int number)
        {
            log.LogInformation($"C# HTTP trigger function processed a request.");

            return (ActionResult)new OkObjectResult($"Number : {number}");
        }
    }
}
