import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Rasyid' },
  { id: 12, name: 'Fauzan' },
  { id: 13, name: 'Deo' },
  { id: 14, name: 'Winston' },
  { id: 15, name: 'Volyando' },
  { id: 16, name: 'Dhanar' }
];