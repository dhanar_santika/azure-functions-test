import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})

export class HeroesComponent implements OnInit {
  
  hero : {} = {
  	id : 1,
  	name : ""
  }

  baseUrl : string = "http://localhost:7071/"
  strOut : string = ""

  constructor(private httpClient: HttpClient){}

  get_greetings(name : string){
    var out = this.httpClient.get(this.baseUrl + 'api/Name?name=' + name, {responseType : 'text'}).subscribe((res)=>{
        console.log(res);
        this.strOut = res;
    });
  }

  ngOnInit() {
  }

}
